import React from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { Button } from 'react-native';

GoogleSignin.configure({
  webClientId: '1047082541758-dpgabifbkkbm0g3ebcn1mu02q7saes8j.apps.googleusercontent.com',
});

const App = () => {

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.body}>
        <Text>Welcome to the AdMob !</Text>
        <Button
          title="Sign in with Google"
          onPress={signIn}/>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center', 
  },
});

const signIn = async () => {
  try {
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    console.log(userInfo);
  } catch (error) {
    console.error(error);
  }
};

export default App;
